package com.vasnabilisim.katip.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.antlr.KatipLexer;
import com.vasnabilisim.katip.antlr.KatipParser;
import com.vasnabilisim.katip.antlr.KatipParser.KatipAttributeContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipChardataContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipDocumentContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipElementContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipExpressionFunctionContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipExpressionLiteralContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipExpressionVariableContext;
import com.vasnabilisim.katip.antlr.KatipParser.KatipExpressionVariableNameContext;
import com.vasnabilisim.katip.antlr.KatipParserBaseListener;
import com.vasnabilisim.katip.document.expression.ExpressionType;
import com.vasnabilisim.katip.document.expression.Function;
import com.vasnabilisim.katip.document.expression.Literal;
import com.vasnabilisim.katip.document.expression.Variable;

/**
 * @author Menderes Fatih G�VEN
 */
public class DocumentIO {
	
	public static void write(Document document, String fileName) throws KatipException {
		Element rootElement = document.getRoot();
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8")) {
			
			for(Element element : rootElement.getElements())
				write(writer, element);
		} catch (IOException e) {
			throw new KatipException(e);
		}
	}

	private static void write(Writer writer, Element element) throws IOException { 
		if(element instanceof Text)
			write(writer, (Text)element);
		else {
			writer.append('<');
			writer.append(element.getName());
			for(Attribute attribute : element.getAttributes()) {
				writer.append(' ');
				writer.append(attribute.getName());
				writer.append('=');
				writer.append('"');
				writer.append(String.valueOf(attribute.getExpression()));
				writer.append('"');
			}
			if(element.getElements().isEmpty()) {
				writer.append("/>");
			}
			else {
				writer.append(">");
				for(Element childElement : element.getElements())
					write(writer, childElement);
				writer.append("</");
				writer.append(element.getName());
				writer.append(">");
			}
		}
	}

	private static void write(Writer writer, Text text) throws IOException { 
		writer.append(text.getText());
	}

	public static Document read(String fileName) throws KatipException {
		ANTLRFileStream in;
		try {
			in = new ANTLRFileStream(fileName, "UTF-8");
		} catch (IOException e) {
			throw new KatipException(e);
		}
		KatipLexer lexer = new KatipLexer(in);
		KatipParser parser = new KatipParser(new CommonTokenStream(lexer));
		KatipDocumentContext document = parser.katipDocument();
		
		ParseTreeWalker walker = new ParseTreeWalker();
		
		Listener listener = new Listener(fileName);
	    walker.walk(listener, document);
	    
	    return listener.document;
	}

	public static Document read(File file) throws KatipException {
		return read(file.getPath());
	}
	
	static class Listener extends KatipParserBaseListener {
		
		String fileName = null;
		Document document = null;
		LinkedList<Element> elements = null;
		Attribute attribute = null;
		LinkedList<Function> functions = null;
		
		Listener(String fileName) {
			this.fileName = fileName;
		}
		
		@Override
		public void enterKatipDocument(KatipDocumentContext ctx) {
			document = new Document();
			Element rootElement = new Element();
			document.setRoot(rootElement);
			
			elements = new LinkedList<>();
			elements.addLast(rootElement);
		}
		
		@Override
		public void enterKatipElement(KatipElementContext ctx) {
			Element parentElement = elements.getLast();
			Element element = new Element("Root");
			Token token = ctx.katipTagName(0).TAG_NAME().getSymbol();
			element.setName(token.getText());
			element.setFile(fileName);
			element.setLine(token.getLine());
			element.setColumn(token.getCharPositionInLine() + 1);
			parentElement.addElement(element);
			elements.addLast(element);
		}
		
		@Override
		public void exitKatipElement(KatipElementContext ctx) {
			elements.removeLast();
		}
		
		@Override
		public void enterKatipAttribute(KatipAttributeContext ctx) {
			Element parentElement = elements.getLast();
			attribute = new Attribute();
			Token token = ctx.katipAttributeName().TAG_NAME().getSymbol();
			attribute.setName(token.getText());
			attribute.setFile(fileName);
			attribute.setLine(token.getLine());
			attribute.setColumn(token.getCharPositionInLine() + 1);
			parentElement.addAttribute(attribute);
			
			functions = new LinkedList<>();
			Function rootFunction = new Function("Root");
			functions.add(rootFunction);
		}
		
		@Override
		public void exitKatipAttribute(KatipAttributeContext ctx) {
			attribute.setExpression(functions.removeLast().getParameters().get(0));
			attribute = null;
			functions = null;
		}
		
		@Override
		public void enterKatipExpressionLiteral(KatipExpressionLiteralContext ctx) {
			Literal literal = null;
			if(ctx.EXPRESSION_STRING_LITERAL() != null) {
				String text = ctx.EXPRESSION_STRING_LITERAL().getSymbol().getText();
				String value = text.substring(1, text.length() - 1);
				literal = new Literal(ExpressionType.String, value);
			}
			else if(ctx.EXPRESSION_BOOLEAN_LITERAL() != null) {
				String text = ctx.EXPRESSION_BOOLEAN_LITERAL().getSymbol().getText();
				Boolean value = Boolean.valueOf(text);
				literal = new Literal(ExpressionType.Boolean, value);
			}
			else if(ctx.EXPRESSION_DECIMAL_LITERAL() != null) {
				String text = ctx.EXPRESSION_DECIMAL_LITERAL().getSymbol().getText();
				if(text.indexOf('.') >= 0)
					literal = new Literal(ExpressionType.Decimal, new BigDecimal(text));
				else 
					literal = new Literal(ExpressionType.Integer, Integer.valueOf(text));
			}
			else {
				literal = new Literal(ExpressionType.Any, null);
			}
			Function function = functions.getLast();
			function.addParameter(literal);
		}
		
		@Override
		public void enterKatipExpressionVariable(KatipExpressionVariableContext ctx) {
			List<KatipExpressionVariableNameContext> ctxVariableNameContexts = ctx.katipExpressionVariableName();
			String[] names = new String[ctxVariableNameContexts.size()];
			for(int index=0; index<names.length; ++index)
				names[index] = ctxVariableNameContexts.get(index).EXPRESSION_NAME().getSymbol().getText();
			Variable variable = new Variable(names);
			Function function = functions.getLast();
			function.addParameter(variable);
		}
		
		@Override
		public void enterKatipExpressionFunction(KatipExpressionFunctionContext ctx) {
			Function newFunction = new Function(ctx.katipFunctionName().EXPRESSION_NAME().getSymbol().getText());
			Function function = functions.getLast();
			function.addParameter(newFunction);
			functions.addLast(newFunction);
		}

		@Override
		public void exitKatipExpressionFunction( KatipExpressionFunctionContext ctx) {
			functions.removeLast();
		}
		
		@Override
		public void enterKatipChardata(KatipChardataContext ctx) {
			TerminalNode node = ctx.TEXT();
			if(node == null)
				node = ctx.SEA_WS();
			if(node != null) {
				Element parentElement = elements.getLast();
				Text text = new Text();
				Token token = node.getSymbol();
				text.setText(token.getText());
				text.setFile(fileName);
				text.setLine(token.getLine());
				text.setColumn(token.getCharPositionInLine() + 1);
				parentElement.addElement(text);
			}
		}
	}
}
