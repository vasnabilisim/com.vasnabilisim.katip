package com.vasnabilisim.katip.document;

/**
 * @author Menderes Fatih G�VEN
 */
public class Text extends Element {

	CharSequence text;
	
	public Text() {
		super();
	}

	public Text(CharSequence text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return text == null ? null : text.toString();
	}

	public CharSequence getText() {
		return text;
	}
	
	public void setText(CharSequence text) {
		this.text = text;
	}
}
