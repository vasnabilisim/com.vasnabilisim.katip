package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Format extends Function {

	public Format() {
		super(ExpressionType.String, "Format");
	}
	
	@Override
	public int getMinParameterCount() {
		return 1;
	}
	
	@Override
	public int getMaxParameterCount() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}

	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		String format = parameters[0] == null ? null : parameters[0].toString();
		if(format == null)
			return null;
		Object[] args = new Object[parameters.length -1];
		for(int index=1; index<parameters.length; ++index)
			args[index-1] = parameters[index];
		return String.format(format, args);
	}
}
