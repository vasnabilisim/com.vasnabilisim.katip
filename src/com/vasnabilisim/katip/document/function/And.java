package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class And extends Function {

	public And() {
		super(ExpressionType.Boolean, "And");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}

	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		for(int index = 0; index<parameters.length; ++index) {
			if(parameters[index] == null)
				return false;
			if(!(Boolean)parameters[index])
				return false;
		}
		return true;
	}
}
