package com.vasnabilisim.katip.document.function;

import java.util.List;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class ItemAt extends Function {

	public ItemAt() {
		super(ExpressionType.Any, "ItemAt");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 2;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		switch (index) {
		case 0: return ExpressionType.List;
		case 1: return ExpressionType.Integer;
		default: return ExpressionType.Any;
		}
	}

	@Override
	public Object eval(Object... parameters) {
		List<?> list = (List<?>) parameters[0];
		Integer index = (Integer) parameters[1];
		if(list == null || index == null)
			return null;
		if(index < 0 || index >= list.size())
			return null;
		return list.get(index);
	}
}
