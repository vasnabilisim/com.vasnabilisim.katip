package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class LowerCase extends Function {

	public LowerCase() {
		super(ExpressionType.String, "LowerCase");
	}
	
	@Override
	public int getMinParameterCount() {
		return 1;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 1;
	}

	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}
	
	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		String value = parameters[0] == null ? null : parameters[0].toString();
		if(value == null)
			return null;
		return value.toLowerCase();
	}
}
