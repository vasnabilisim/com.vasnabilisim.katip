package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Equals extends Function {

	public Equals() {
		super(ExpressionType.Boolean, "Equals");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 2;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}

	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		String lhs = parameters[0] == null ? null : parameters[0].toString();
		String rhs = parameters[1] == null ? null : parameters[1].toString();
		if(lhs == null)
			return rhs == null;
		if(rhs == null)
			return Boolean.FALSE;
		return lhs.equals(rhs);
	}
}
