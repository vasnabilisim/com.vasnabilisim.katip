package com.vasnabilisim.katip.document.function;

import java.util.Random;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class RandomLong extends Function {

	public RandomLong() {
		super(ExpressionType.Decimal, "RandomLong");
	}
	
	@Override
	public int getMinParameterCount() {
		return 0;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 0;
	}

	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}
	
	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		return random.nextLong();
	}
	
	Random random = new Random();
}
