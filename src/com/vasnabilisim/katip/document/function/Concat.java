package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Concat extends Function {

	public Concat() {
		super(ExpressionType.String, "Concat");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}

	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		String result = "";
		for(Object parameter : parameters)
			if(parameter != null)
				result += parameter;
		return result;
	}
}
