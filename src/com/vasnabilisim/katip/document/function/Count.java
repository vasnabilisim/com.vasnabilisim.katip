package com.vasnabilisim.katip.document.function;

import java.util.Collection;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Count extends Function {

	public Count() {
		super(ExpressionType.Integer, "Count");
	}
	
	@Override
	public int getMinParameterCount() {
		return 1;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 1;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Collection;
	}

	@Override
	public Object eval(Object... parameters) {
		Collection<?> collection = (Collection<?>) parameters[0];
		if(collection == null)
			return 0;
		return collection.size();
	}
}
