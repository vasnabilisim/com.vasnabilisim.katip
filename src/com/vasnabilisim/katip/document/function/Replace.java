package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class Replace extends Function {

	public Replace() {
		super(ExpressionType.String, "Replace");
	}
	
	@Override
	public int getMinParameterCount() {
		return 3;
	}
	
	@Override
	public int getMaxParameterCount() {
		return 3;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Any;
	}

	/**
	 * @see com.vasnabilisim.katip.document.function.Function#eval(java.lang.Object[])
	 */
	@Override
	public Object eval(Object... parameters) {
		String value = parameters[0] == null ? null : parameters[0].toString();
		String target = parameters[1] == null ? null : parameters[1].toString();
		String replacement = parameters[2] == null ? "" : parameters[2].toString();
		if(value == null)
			return null;
		if(target == null)
			return value;
		return value.replace(target, replacement);
	}
}
