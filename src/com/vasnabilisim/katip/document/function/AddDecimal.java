package com.vasnabilisim.katip.document.function;

import java.math.BigDecimal;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class AddDecimal extends Function {

	public AddDecimal() {
		super(ExpressionType.Decimal, "AddDecimal");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Decimal;
	}

	@Override
	public Object eval(Object... parameters) {
		BigDecimal sum = BigDecimal.ZERO;
		for(Object param : parameters) {
			if(param == null)
				continue;
			sum = sum.add((BigDecimal)param);
		}
		return sum;
	}
}
