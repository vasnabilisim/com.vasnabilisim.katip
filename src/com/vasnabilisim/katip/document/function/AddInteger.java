package com.vasnabilisim.katip.document.function;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public class AddInteger extends Function {

	public AddInteger() {
		super(ExpressionType.Integer, "AddInteger");
	}
	
	@Override
	public int getMinParameterCount() {
		return 2;
	}
	
	@Override
	public int getMaxParameterCount() {
		return Integer.MAX_VALUE;
	}
	
	@Override
	public ExpressionType getParameterType(int index) {
		return ExpressionType.Integer;
	}

	@Override
	public Object eval(Object... parameters) {
		int sum = 0;
		for(Object param : parameters) {
			if(param == null)
				continue;
			sum += (Integer)param;
		}
		return sum;
	}
}
