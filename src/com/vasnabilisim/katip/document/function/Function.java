package com.vasnabilisim.katip.document.function;

import java.util.TreeMap;

import com.vasnabilisim.katip.document.expression.ExpressionType;

/**
 * @author Menderes Fatih G�VEN
 */
public abstract class Function {

	static final TreeMap<String, Function> FUNCTIONS = new TreeMap<>();
	
	public static Function get(String name) {
		return FUNCTIONS.get(name);
	}
	
	public static void register(Function function) {
		FUNCTIONS.put(function.getName(), function);
	}
	
	static {
		register(new AddInteger());
		register(new AddDecimal());
		register(new And());
		register(new Concat());
		register(new Count());
		register(new Equals());
		register(new Format());
		register(new Concat());
		register(new IsEmpty());
		register(new ItemAt());
		register(new LowerCase());
		register(new LowerFirstChar());
		register(new Not());
		register(new Or());
		register(new RandomLong());
		register(new Replace());
		register(new UpperCase());
	}
	
	protected ExpressionType returnType;
	protected String name;

	public Function(ExpressionType returnType, String name) {
		this.returnType = returnType;
		this.name = name;
	}

	public ExpressionType getReturnType() {
		return returnType;
	}
	
	public String getName() {
		return name;
	}
	
	String _toString;
	@Override
	public String toString() {
		if(_toString == null) {
			StringBuilder builder = new StringBuilder(100);
			builder.append(name).append('(');
			boolean comma = false;
			int index = 0;
			for( ;index<getMinParameterCount(); ++index) {
				if(comma)
					builder.append(",");
				builder.append(getParameterType(index).name());
				comma = true;
			}
			if(getMaxParameterCount() == Integer.MAX_VALUE) {
				if(comma)
					builder.append(",");
				builder.append(getParameterType(index).name()).append("...");
			}
			else {
				for( ;index<getMaxParameterCount(); ++index) {
					if(comma)
						builder.append(",");
					builder.append(getParameterType(index).name());
					comma = true;
				}
			}
			builder.append(')');
			_toString = builder.toString();
		}
		return _toString;		
	}
	
	public abstract int getMinParameterCount();
	
	public abstract int getMaxParameterCount();
	
	public abstract ExpressionType getParameterType(int index);
	
	public abstract Object eval(Object... parameters);
	
	static class FunctionKey implements Comparable<FunctionKey> {
		String name;
		int parameterCount;
		FunctionKey(String name, int parameterCount) {
			this.name = name;
			this.parameterCount = parameterCount;
		}
		public int compareTo(FunctionKey other) {
			int result = this.name.compareTo(other.name);
			return result == 0 ? Integer.compare(this.parameterCount, other.parameterCount) : result;
		}
	}
}
