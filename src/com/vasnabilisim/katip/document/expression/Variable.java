package com.vasnabilisim.katip.document.expression;

import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.reflect.ReflectionException;
import com.vasnabilisim.reflect.ReflectionUtil;

/**
 * @author Menderes Fatih G�VEN
 */
public class Variable extends Expression {

	String names[] = null;
	String fullName = null;
	
	public Variable() {
		super(ExpressionType.Any);
	}
	
	public Variable(String names[]) {
		super(ExpressionType.Any);
		setNames(names);
	}
	
	@Override
	public String toString() {
		return fullName;
	}
	
	public String[] getNames() {
		return names;
	}
	
	public void setNames(String[] names) {
		this.names = names;
		this.fullName = null;
		if(names != null) {
			StringBuilder builder = new StringBuilder(names.length * 15);
			boolean dot = false;
			for(int index=0; index<names.length; ++index) {
				if(dot)
					builder.append('.');
				builder.append(names[index]);
				dot = true;
			}
			this.fullName = builder.toString();
		}
	}

	@Override
	public Object eval(Variables variables) {
		if(names == null || names.length == 0)
			return null;
		Object value = variables.get(names[0]);
		for(int index=1; index<names.length && value != null; ++index) {
			try {
				value = ReflectionUtil.invoke(value.getClass(), Object.class, "get" + names[index], value);
			} catch (ReflectionException e) {
				value = null;
			}
		}
		setType(ExpressionType.valueOf(value));
		return value;
	}

}
