package com.vasnabilisim.katip.document.expression;

public enum ExpressionType {

	String(java.lang.String.class), 
	Integer(java.lang.Integer.class), 
	Decimal(java.math.BigDecimal.class), 
	Boolean(java.lang.Boolean.class), 
	List(java.util.List.class), 
	Collection(java.util.Collection.class), 
	Any(java.lang.Object.class),;

	Class<?> typeClass;
	
	ExpressionType(Class<?> typeClass) {
		this.typeClass = typeClass;
	}
	
	public Class<?> getTypeClass() {
		return typeClass;
	}
	
	public boolean isInstance(Object value) {
		return value == null || typeClass.isInstance(value);
	}
	
	public static ExpressionType valueOf(Object value) {
		if(value == null)
			return Any;
		for(ExpressionType type : values()) 
			if(type.isInstance(value))
				return type;
		return Any;
	}
}
