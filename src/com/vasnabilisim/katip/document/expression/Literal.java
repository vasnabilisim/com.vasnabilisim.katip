package com.vasnabilisim.katip.document.expression;

import com.vasnabilisim.katip.Variables;

/**
 * @author Menderes Fatih G�VEN
 */
public class Literal extends Expression {

	Object value = null;
	
	public Literal(ExpressionType type) {
		super(type);
	}
	
	public Literal(ExpressionType type, Object value) {
		super(type);
		setValue(value);
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		if(type.isInstance(value))
			this.value = value;
		else
			this.value = null;
	}

	@Override
	public Object eval(Variables variables) {
		return value;
	}

	@Override
	public String toString() {
		switch (type) {
		case String:	return "'" + value + "'";
		case Decimal:	return String.valueOf(value);
		case Boolean:	return String.valueOf(value);
		default:		return String.valueOf(value);
		}
	}
}
