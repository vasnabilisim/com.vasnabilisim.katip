package com.vasnabilisim.katip.document.expression;

import java.util.ArrayList;

import com.vasnabilisim.katip.Variables;

/**
 * @author Menderes Fatih G�VEN
 */
public class Function extends Expression {

	String name = null;
	ArrayList<Expression> parameters = new ArrayList<Expression>(3);
	
	public Function() {
		super(ExpressionType.Any);
	}
	
	public Function(String name) {
		super(ExpressionType.Any);
		setName(name);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(100);
		builder.append(name);
		builder.append('(');
		boolean comma = false;
		for(Expression parameter : parameters) {
			if(comma)
				builder.append(',');
			builder.append(parameter.toString());
			comma = true;
		}
		builder.append(')');
		return builder.toString();
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getParameterCount() {
		return parameters.size();
	}
	
	public ArrayList<Expression> getParameters() {
		return parameters;
	}
	
	public void addParameter(Expression parameter) {
		parameters.add(parameter);
	}

	@Override
	public Object eval(Variables variables) {
		com.vasnabilisim.katip.document.function.Function function = com.vasnabilisim.katip.document.function.Function.get(name);
		if(function == null)
			return null;
		setType(function.getReturnType());
		ArrayList<Object> parameterValues = new ArrayList<Object>(parameters.size());
		for(Expression parameter : parameters)
			parameterValues.add(parameter.eval(variables));
		return function.eval(parameterValues.toArray());
	}

}
