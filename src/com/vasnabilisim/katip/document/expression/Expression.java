package com.vasnabilisim.katip.document.expression;

import com.vasnabilisim.katip.Variables;

/**
 * @author Menderes Fatih G�VEN
 */
public abstract class Expression {

	protected ExpressionType type;
	
	public Expression(ExpressionType type) {
		this.type = type;
	}

	public ExpressionType getType() {
		return type;
	}
	
	public void setType(ExpressionType type) {
		this.type = type;
	}
	
	public abstract Object eval(Variables variables);
}
