package com.vasnabilisim.katip.document;

/**
 * @author Menderes Fatih G�VEN
 */
public class Document {

	Element root;
	
	public Document() {
	}
	
	public Element getRoot() {
		return root;
	}
	
	public void setRoot(Element root) {
		this.root = root;
	}
}
