package com.vasnabilisim.katip.document;

/**
 * @author Menderes Fatih G�VEN
 */
public abstract class Node {

	Element parent;
	CharSequence name;
	String file;
	int line;
	int column;
	
	public Node() {
	}
	
	public Node(CharSequence name) {
		this.name = name;
	}
	
	public Element getParent() {
		return parent;
	}
	
	public void setParent(Element parent) {
		this.parent = parent;
	}
	
	@Override
	public String toString() {
		return name == null ? null : name.toString();
	}
	
	public CharSequence getName() {
		return name;
	}
	
	public void setName(CharSequence name) {
		this.name = name;
	}
	
	public String getFile() {
		return file;
	}
	
	public void setFile(String file) {
		this.file = file;
	}
	
	public int getLine() {
		return line;
	}
	
	public void setLine(int line) {
		this.line = line;
	}
	
	public int getColumn() {
		return column;
	}
	
	public void setColumn(int column) {
		this.column = column;
	}
}
