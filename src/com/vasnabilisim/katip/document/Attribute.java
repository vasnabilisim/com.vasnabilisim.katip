package com.vasnabilisim.katip.document;

import com.vasnabilisim.katip.document.expression.Expression;

/**
 * @author Menderes Fatih G�VEN
 */
public class Attribute extends Node {

	Expression expression;
	
	/**
	 * 
	 */
	public Attribute() {
	}

	/**
	 * @param name
	 */
	public Attribute(String name) {
		super(name);
	}
	
	public Expression getExpression() {
		return expression;
	}
	
	public void setExpression(Expression expression) {
		this.expression = expression;
	}
}
