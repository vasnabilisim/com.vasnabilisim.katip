package com.vasnabilisim.katip.document;

import java.util.ArrayList;

/**
 * @author Menderes Fatih G�VEN
 */
public class Element extends Node {

	boolean block = false;
	ArrayList<Element> elements = new ArrayList<>();
	ArrayList<Attribute> attributes = new ArrayList<>(3);

	public Element() {
	}

	public Element(CharSequence name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "<%" + name + "%>";
	}
	
	public boolean isBlock() {
		return block;
	}
	
	public void setBlock(boolean block) {
		this.block = block;
	}

	public ArrayList<Element> getElements() {
		return elements;
	}
	
	public void addElement(Element element) {
		element.setParent(this);
		elements.add(element);
	}
	
	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}
	
	public void addAttribute(Attribute attribute) {
		attribute.setParent(this);
		attributes.add(attribute);
	}
}
