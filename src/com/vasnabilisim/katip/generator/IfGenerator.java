package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;

/**
 * @author Menderes Fatih G�VEN
 */
public class IfGenerator extends BlockGenerator {

	public IfGenerator() {
		super("If");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		Expression expression = getAttributeExpression(element, "Expression");
		if(expression == null) {
			Logger.error(String.format("Invalid Expression in <If> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		Object value = expression.eval(variables);
		if(value == null)
			return true;
		boolean condition = true;
		if(value instanceof Boolean)
			condition = (Boolean)value;
		else if(value instanceof Iterable)
			condition = ((Iterable<?>)value).iterator().hasNext();
		if(condition)
			return super.generate(element, variables, sourceFolder, targetFolder, writer);
		return true;
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Expression");
	}
}
