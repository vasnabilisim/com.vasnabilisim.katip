package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Document;
import com.vasnabilisim.katip.document.DocumentIO;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;

/**
 * @author Menderes Fatih G�VEN
 */
public class ImportGenerator extends BlockGenerator {

	public ImportGenerator() {
		super("Import");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		Expression expression = getAttributeExpression(element, "File");
		if(expression == null) {
			Logger.error(String.format("Invalid File in <Import> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		Object value = expression.eval(variables);
		if(value == null) {
			Logger.error(String.format("Invalid File in <Import> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		String templateFileName = value.toString();
		File templateFile = new File(sourceFolder, templateFileName);
		Document document;
		try {
			document = DocumentIO.read(templateFile);
		} catch (KatipException e) {
			Logger.error(String.format("Invalid File in <Import> at %s:%d. Unable to read document %s.", element.getFile(), element.getLine(), templateFile.getPath()));
			Logger.log(e);
			return false;
		}
		return super.generate(document.getRoot(), variables, sourceFolder, targetFolder, writer);
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "File");
	}
}
