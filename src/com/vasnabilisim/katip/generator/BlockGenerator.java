package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.Text;

/**
 * @author Menderes Fatih G�VEN
 */
public class BlockGenerator extends Generator {

	public BlockGenerator() {
		super("Block");
	}

	public BlockGenerator(String name) {
		super(name);
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		for(Element childElement : element.getElements()) {
			if(childElement instanceof Text) {
				if(writer != null) {
					Text text = (Text) childElement;
					if(!write(writer, text.getText()))
						return false;
				}
				continue;
			}
			CharSequence childElementName = childElement.getName();
			Generator childGenerator = get(childElementName);
			if(childGenerator == null) {
				Logger.error(String.format("Unable to find generator for %s", childElementName));
				return false;
			}
			if(!childGenerator.generate(childElement, variables, sourceFolder, targetFolder, writer))
				return false;
		}
		return true;
	}
	
	@Override
	public void validate(Element element) throws KatipException {
	}
}
