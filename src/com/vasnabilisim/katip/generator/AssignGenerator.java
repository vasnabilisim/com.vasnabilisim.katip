package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;
import com.vasnabilisim.katip.document.expression.Variable;

/**
 * @author Menderes Fatih G�VEN
 */
public class AssignGenerator extends Generator {

	/**
	 * @param name
	 */
	public AssignGenerator() {
		super("Assign");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File parentFolder, Writer writer) {
		Expression expression = getAttributeExpression(element, "Variable");
		if(!(expression instanceof Variable))
			return false;
		Variable variable = (Variable) expression;
		if(variable.getNames() == null || variable.getNames().length != 1) {
			Logger.error(String.format("Invalid Variable in <Assign> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		String variableName = variable.getNames()[0];
		
		expression = getAttributeExpression(element, "Expression");
		if(expression == null) {
			variables.put(variableName, null);
			return true;
		}
		Object value = expression.eval(variables);
		variables.put(variableName, value);
		return true;
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Variable");
		validateAttribute(element, "Expression");
	}
}
