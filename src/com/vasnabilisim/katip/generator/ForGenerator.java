package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;
import java.util.Iterator;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;
import com.vasnabilisim.katip.document.expression.Variable;

/**
 * @author Menderes Fatih G�VEN
 */
public class ForGenerator extends BlockGenerator {

	/**
	 * @param name
	 */
	public ForGenerator() {
		super("For");
	}

	/**
	 * @see com.vasnabilisim.katip.generator.Generator#generate(com.vasnabilisim.katip.document.Element, java.util.Map, java.io.File, java.io.Writer)
	 */
	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		Expression expression = getAttributeExpression(element, "Variable");
		if(!(expression instanceof Variable))
			return false;
		Variable variable = (Variable) expression;
		if(variable.getNames() == null || variable.getNames().length != 1) {
			Logger.error(String.format("Invalid Variable name in <For> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		String variableName = variable.getNames()[0];
		
		expression = getAttributeExpression(element, "CollectionExpression");
		if(expression == null)
			return false;
		Object value = expression.eval(variables);
		if(value == null)
			return false;
		if(!(value instanceof Iterable)) {
			Logger.error(String.format("Invalid CollectionExpression in <For> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		
		Iterable<?> iterable = (Iterable<?>) value;
		Iterator<?> iterator = iterable.iterator();
		Object oldValue = variables.get(variableName);
		boolean result = true;
		while(iterator.hasNext()) {
			Object item = iterator.next();
			variables.put(variableName, item);
			result &= super.generate(element, variables, sourceFolder, targetFolder, writer);
		}
		variables.remove(variableName);
		if(oldValue != null)
			variables.put(variableName, oldValue);
		return result;
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Variable");
		validateAttribute(element, "CollectionExpression");
	}
}
