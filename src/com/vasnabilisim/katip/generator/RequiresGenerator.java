package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;
import com.vasnabilisim.katip.document.expression.Variable;

/**
 * @author Menderes Fatih G�VEN
 */
public class RequiresGenerator extends Generator {

	public RequiresGenerator() {
		super("Requires");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		Expression expression = getAttributeExpression(element, "Variable");
		if(!(expression instanceof Variable))
			return false;
		Variable variable = (Variable) expression;
		if(variable.getNames() == null || variable.getNames().length != 1) {
			Logger.error(String.format("Invalid Variable in <Resquires> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		String variableName = variable.getNames()[0];
		if(variables.containsKey(variableName))
			return true;
		expression = getAttributeExpression(element, "Default");
		if(expression == null)
			return false;
		Object defaultValue = expression.eval(variables);
		if(defaultValue == null)
			return false;
		variables.put(variableName, defaultValue);
		return true;
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Variable");
	}
}
