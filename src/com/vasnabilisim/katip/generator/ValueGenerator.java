package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.Writer;

import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;

/**
 * @author Menderes Fatih G�VEN
 */
public class ValueGenerator extends Generator {

	public ValueGenerator() {
		super("Value");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		if(writer == null)
			return false;
		Object value = getAttributeValue(element, "Expression", variables);
		if(value == null)
			return true;
		return write(writer, value);
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Expression");
	}
}
