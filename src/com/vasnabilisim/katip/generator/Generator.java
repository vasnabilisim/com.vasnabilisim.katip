package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.TreeMap;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Attribute;
import com.vasnabilisim.katip.document.Document;
import com.vasnabilisim.katip.document.DocumentIO;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.expression.Expression;

/**
 * @author Menderes Fatih G�VEN
 */
public abstract class Generator {
	
	static final TreeMap<CharSequence, Generator> GENERATORS = new TreeMap<>();
	
	static {
		register(new AssignGenerator());
		register(new FileGenerator());
		register(new ForGenerator());
		register(new IfGenerator());
		register(new ImportGenerator());
		register(new RequiresGenerator());
		register(new ValueGenerator());
	}
	
	public static void register(Generator generator) {
		GENERATORS.put(generator.getName(), generator);
	}
	
	public static Generator get(CharSequence name) {
		return GENERATORS.get(name);
	}
	
	public static void generate(String sourceFolderName, String templateFileName, String targetFolderName, Variables variables) throws KatipException {
		File sourceFolder = new File(sourceFolderName);
		File templateFile = new File(sourceFolder, templateFileName);
		Document document = DocumentIO.read(templateFile);
		
		File targetFolder = new File(targetFolderName);
		BlockGenerator generator = new BlockGenerator();
		generator.generate(document.getRoot(), variables, sourceFolder, targetFolder, null);
	}

	protected CharSequence name;
	
	public Generator(CharSequence name) {
		this.name = name;
	}
	
	public CharSequence getName() {
		return name;
	}
	
	public abstract boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer);

	protected boolean write(Writer writer, Object value) {
		if(writer == null || value == null)
			return false;
		try {
			writer.write(value.toString());
			return true;
		} catch (IOException e) {
			Logger.log(e);
			return false;
		}
	}
	
	public abstract void validate(Element element) throws KatipException;


	protected void validateAttribute(Element element, String attributeName) throws KatipException {
		for(Attribute attribute : element.getAttributes())
			if(attributeName.equals(attribute.getName()))
				return;
		throw new KatipException(String.format("Unable to find attribute %s in %s", attributeName, element.getName()));
	}
	
	protected Expression getAttributeExpression(Element element, String attributeName) {
		for(Attribute attribute : element.getAttributes())
			if(attributeName.equals(attribute.getName()))
				return attribute.getExpression();
		return null;
	}
	
	protected Object getAttributeValue(Element element, String attributeName, Variables variables) {
		for(Attribute attribute : element.getAttributes())
			if(attributeName.equals(attribute.getName()))
				return attribute.getExpression().eval(variables);
		return null;
	}
}
