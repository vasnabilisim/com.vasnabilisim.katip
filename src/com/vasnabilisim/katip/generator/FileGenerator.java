package com.vasnabilisim.katip.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.vasnabilisim.core.Logger;
import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.Variables;
import com.vasnabilisim.katip.document.Element;

public class FileGenerator extends BlockGenerator {

	public FileGenerator() {
		super("File");
	}

	@Override
	public boolean generate(Element element, Variables variables, File sourceFolder, File targetFolder, Writer writer) {
		Object value = getAttributeValue(element, "Expression", variables);
		if(value == null) {
			Logger.error(String.format("Invalid Expression in <File> at %s:%d", element.getFile(), element.getLine()));
			return false;
		}
		String fileName = value.toString();
		File file = new File(targetFolder, fileName);
		File folder = file.getParentFile();
		if(!folder.exists())
			folder.mkdirs();
		try(OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))
		{
			return super.generate(element, variables, sourceFolder, targetFolder, fileWriter);
		} catch (IOException e) {
			return false;
		}
	}
	
	@Override
	public void validate(Element element) throws KatipException {
		validateAttribute(element, "Expression");
	}
}
