package com.vasnabilisim.katip;

import java.util.TreeMap;

/**
 * @author Menderes Fatih G�VEN
 */
public class Variables extends TreeMap<String, Object> {
	private static final long serialVersionUID = -1890530042631394518L;

	public Variables() {
	}

	@Override
	public Object put(String key, Object value) {
		if(value == null)
			return super.remove(key);
		return super.put(key, value);
	}
}
