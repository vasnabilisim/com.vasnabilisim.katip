package com.vasnabilisim.katip.old;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.document.Document;
import com.vasnabilisim.katip.document.Element;
import com.vasnabilisim.katip.document.Text;

/**
 * @author Menderes Fatih G�VEN
 */
public class ParserOld {

	public ParserOld() {
	}

	public Document parse(String fileName) throws KatipException {
		return parse(new File(fileName));
	}

	public Document parse(File file) throws KatipException {
		try {
			return parse(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new KatipException(e);
		}
	}
	
	public Document parse(InputStream in) throws KatipException {
		try {
			return parse(new InputStreamReader(in, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new KatipException(e);
		}
	}

	public Document parse(Reader reader) throws KatipException {
		return parseInternal(new BufferedReader(reader));
	}
	
	Document parseInternal(Reader reader) throws KatipException {
		Element rootElement = new Element("Root");
		
		CharIterator iterator;
		try {
			iterator = new CharIterator(reader);
		} catch (IOException e) {
			throw new KatipException(e);
		}
		
		try {
			while(!iterator.isEOF()) {
				parseLine(iterator, rootElement);
			}
		} catch (IOException e) {
			throw new KatipException(String.format("Unable to parse element at line %d", iterator.getLine()));
		}
		
		Document document = new Document();
		document.setRoot(rootElement);
		return document;
	}
	
	

	void parseLine(CharIterator iterator, Element parentElement) throws IOException, KatipException {
		StringBuilder buffer = new StringBuilder();
		while(!iterator.isEOL()) {
			char ch1 = iterator.nextChar();
			if(ch1 == '<') {
				if(iterator.hasNext()) {
					char ch2 = iterator.nextChar();
					if(ch2 == '%') {
						if(buffer.length() > 0) {
							Text text = new Text(buffer);
							parentElement.addElement(text);
							buffer = new StringBuilder();
						}
						parseElement(iterator, parentElement);
					}
					else
						buffer.append(ch1);
				}
			}
			else
				buffer.append(ch1);
		}
	}

	@SuppressWarnings("unused")
	void parseElement(CharIterator iterator, Element rootElement) throws IOException, KatipException {
		if(iterator.isEOL())
			throw new KatipException(String.format("Unable to parse element at line %d", iterator.getLine()));
		CharSequence elementName = iterator.nextElementName();
		boolean blockElement = isBlockElement(elementName);
		Element element = new Element(elementName);
		char ch1 = iterator.nextChar();
		char ch2 = iterator.nextChar();
		while(ch1 != '%' || ch2 != '>') {
			CharSequence attributeName = iterator.nextElementName();
		}
	}
	
	boolean isBlockElement(CharSequence elementName) {
		return false;
	}
}
