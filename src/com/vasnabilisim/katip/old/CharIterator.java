package com.vasnabilisim.katip.old;

import java.io.IOException;
import java.io.Reader;

/**
 * @author Menderes Fatih G�VEN
 */
public class CharIterator{

	Reader reader;
	int next;
	boolean skipLF = false;
	
	int line;
	int column;
	
	public CharIterator(Reader reader) throws IOException {
		this.reader = reader;
		next = reader.read();
		line = 1;
		column = 0;
	}

	public boolean hasNext() throws IOException {
		if(skipLF) {
			if(next == '\n')
				next = reader.read();
			skipLF = false;
		}
		return next >= 0;
	}
	
	public char nextChar() throws IOException {
		if(next < 0)
			throw new IOException("EOF");
		if(next == '\n') {
			++line;
			column = 0;
		}
		else if(next == '\r') {
			++line;
			column = 0;
			skipLF = true;
		}
		char nextChar = (char) next;
		next = reader.read();
		return nextChar;
	}
	
	public CharSequence nextElementName() throws IOException {
		if(next < 0)
			throw new IOException("EOF");
		StringBuilder builder = new StringBuilder();
		while(hasNext() && next != ' ' || next != '\t' || next != '%') {
			builder.append(next);
		}
		return builder;
	}
	
	public CharSequence nextAttributeName() throws IOException {
		if(next < 0)
			throw new IOException("EOF");
		StringBuilder builder = new StringBuilder();
		while(hasNext() && next != ' ' || next != '\t' || next != '=' || next != '%') {
			builder.append(next);
		}
		return builder;
	}
	
	public boolean isEOF() {
		return next < 0;
	}
	
	public boolean isEOL() {
		return next == '\n' || next == '\r';
	}
	
	public boolean isLF() {
		return next == '\n';
	}
	
	public boolean isCR() {
		return next == '\r';
	}
	
	public boolean isS() {
		return next == ' ' || next == '\t';
	}
	
	public boolean isWS() {
		return next == ' ' || next == '\t' || next == '\n' || next == '\r';
	}
	
	public boolean isDelim() {
		return	next == ' ' || 
				next == '\t' || 
				next == '<' || 
				next == '%' || 
				next == '>' || 
				next == '=' || 
				next == '\n' || 
				next == '\r';
	}
	
	public int getLine() {
		return line;
	}
	
	public int getColumn() {
		return column;
	}
}