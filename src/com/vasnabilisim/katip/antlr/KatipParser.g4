parser grammar KatipParser;

options { tokenVocab=KatipLexer; }

katipDocument    
    : katipElements*
    ;

katipElements
    : katipMisc* katipElement katipMisc*
    ;

katipElement     
    : TAG_OPEN katipTagName katipAttribute* TAG_CLOSE katipContent TAG_OPEN TAG_SHARP katipTagName TAG_CLOSE
    | TAG_OPEN katipTagName katipAttribute* TAG_SHARP_CLOSE
    ;

katipTagName
    : TAG_NAME
    ;

katipContent     
    : katipChardata? ((katipElement | katipComment) katipChardata?)*
    ;

katipAttribute   
    : katipAttributeName TAG_EQUALS katipAttributeValue
    ;

katipAttributeName
    : TAG_NAME
    ;

katipAttributeValue
    : katipExpression ATTRIBUTE_VALUE_CLOSE
    ;

katipExpression 
	: katipExpressionVariable 
	| katipExpressionFunction 
	| katipExpressionLiteral 
    ;

katipExpressionVariable 
    : katipExpressionVariableName (EXPRESSION_DOT katipExpressionVariableName)*
    ;

katipExpressionVariableName 
    : EXPRESSION_NAME
    ;

katipExpressionFunction
    : katipFunctionName EXPRESSION_PARANTHESIS_OPEN (katipFunctionParameter (EXPRESSION_COMMA katipFunctionParameter)*)* EXPRESSION_PARANTHESIS_CLOSE
    ;

katipFunctionName 
    : EXPRESSION_NAME
    ;

katipFunctionParameter 
	: katipExpressionVariable 
	| katipExpressionFunction 
	| katipExpressionLiteral 
    ;

katipExpressionLiteral
    : EXPRESSION_BOOLEAN_LITERAL 
    | EXPRESSION_DECIMAL_LITERAL 
    | EXPRESSION_STRING_LITERAL
    ;

katipChardata
    : TEXT 
    | SEA_WS
    ;

katipMisc        
    : katipComment 
    | katipChardata 
    | SEA_WS
    ;

katipComment
    : BLOCK_COMMENT
    | LINE_COMMENT
    ;
