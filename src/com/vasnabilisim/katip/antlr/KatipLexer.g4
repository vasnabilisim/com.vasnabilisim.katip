lexer grammar KatipLexer;

BLOCK_COMMENT
    : '<#!--' .*? '--#>'
    ;

LINE_COMMENT
    : '<#!' .*? ('\r'? '\n')?
    ;

SEA_WS
    :  (' ' | '\t' | '\r'?'\n')+ 
    ;

TAG_OPEN
    : '<#' -> pushMode(MODE_TAG)
    ;
            
TEXT
	: ('<' ~'#')? ~'<'+
    ;   



mode MODE_TAG;

TAG_CLOSE      
    : '#>' -> popMode
    ;

TAG_SHARP_CLOSE     
    : '##>' -> popMode
    ;

TAG_SHARP      
    : '#' 
    ;

TAG_EQUALS     
    : '="' -> pushMode(MODE_ATTRIBUTE)
    ;

TAG_NAME      
    : TAG_NameStartChar TAG_NameChar* 
    ;

TAG_WHITESPACE
    : [ \t\r\n] -> skip 
    ;

fragment
TAG_Digit           
    : [0-9]
    ;

fragment
TAG_NameChar        
    : TAG_NameStartChar
    | '_' 
    | TAG_Digit 
    ;

fragment
TAG_NameStartChar
    :   [a-zA-Z]
    ;


mode MODE_ATTRIBUTE;

ATTRIBUTE_VALUE_CLOSE
    : '"' -> popMode
    ;
/*
ATTRIBUTE_EXPRESSION
    : ~[<\]]+
    ;
*/
EXPRESSION_COMMA
    : ',' 
    ;

EXPRESSION_DOT
    : '.' 
    ;

EXPRESSION_PARANTHESIS_OPEN
    : '(' 
    ;

EXPRESSION_PARANTHESIS_CLOSE
    : ')' 
    ;

EXPRESSION_BOOLEAN_LITERAL
	:	'true'
	|	'false'
	;

EXPRESSION_DECIMAL_LITERAL
	: [+-]? ('0' | EXPRESSION_NON_ZERO_DIGIT EXPRESSION_DIGIT*) ('.' EXPRESSION_DIGIT* EXPRESSION_NON_ZERO_DIGIT+)? 
	;

EXPRESSION_STRING_LITERAL
	: '\'' (~[\'\"\\])* '\''
	;

fragment
EXPRESSION_NON_ZERO_DIGIT		   
	: [1-9]
	;

fragment
EXPRESSION_DIGIT		   
	: '0' 
	| EXPRESSION_NON_ZERO_DIGIT 
	;

	
EXPRESSION_NAME      
    : EXPRESSION_NameStartChar EXPRESSION_NameChar* 
    ;

fragment
EXPRESSION_NameChar        
    : EXPRESSION_NameStartChar
    | '_' 
    | EXPRESSION_DIGIT 
    ;

fragment
EXPRESSION_NameStartChar
    :   [a-zA-Z]
    ;
	