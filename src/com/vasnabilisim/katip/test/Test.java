package com.vasnabilisim.katip.test;

import com.vasnabilisim.katip.KatipException;
import com.vasnabilisim.katip.document.Document;
import com.vasnabilisim.katip.document.DocumentIO;

public class Test {

	public static void main(String[] args) {
		try {
			Document document = DocumentIO.read("template/sql/mysql/table.template");
			DocumentIO.write(document, "template/sql/mysql/table-generated.template");
		} catch (KatipException e) {
			e.printStackTrace();
			System.exit(-1);
			return;
		}
	}

}
