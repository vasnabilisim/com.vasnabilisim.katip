package com.vasnabilisim.katip;

/**
 * @author Menderes Fatih G�VEN
 */
public class KatipException extends Exception {
	private static final long serialVersionUID = -2713304850029846289L;

	/**
	 * 
	 */
	public KatipException() {
	}

	/**
	 * @param message
	 */
	public KatipException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public KatipException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public KatipException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public KatipException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
