// Generated from KatipLexer.g4 by ANTLR 4.4
package com.vasnabilisim.katip.antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KatipLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BLOCK_COMMENT=1, LINE_COMMENT=2, SEA_WS=3, TAG_OPEN=4, TEXT=5, TAG_CLOSE=6, 
		TAG_SHARP_CLOSE=7, TAG_SHARP=8, TAG_EQUALS=9, TAG_NAME=10, TAG_WHITESPACE=11, 
		ATTRIBUTE_VALUE_CLOSE=12, EXPRESSION_COMMA=13, EXPRESSION_DOT=14, EXPRESSION_PARANTHESIS_OPEN=15, 
		EXPRESSION_PARANTHESIS_CLOSE=16, EXPRESSION_BOOLEAN_LITERAL=17, EXPRESSION_DECIMAL_LITERAL=18, 
		EXPRESSION_STRING_LITERAL=19, EXPRESSION_NAME=20;
	public static final int MODE_TAG = 1;
	public static final int MODE_ATTRIBUTE = 2;
	public static String[] modeNames = {
		"DEFAULT_MODE", "MODE_TAG", "MODE_ATTRIBUTE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'"
	};
	public static final String[] ruleNames = {
		"BLOCK_COMMENT", "LINE_COMMENT", "SEA_WS", "TAG_OPEN", "TEXT", "TAG_CLOSE", 
		"TAG_SHARP_CLOSE", "TAG_SHARP", "TAG_EQUALS", "TAG_NAME", "TAG_WHITESPACE", 
		"TAG_Digit", "TAG_NameChar", "TAG_NameStartChar", "ATTRIBUTE_VALUE_CLOSE", 
		"EXPRESSION_COMMA", "EXPRESSION_DOT", "EXPRESSION_PARANTHESIS_OPEN", "EXPRESSION_PARANTHESIS_CLOSE", 
		"EXPRESSION_BOOLEAN_LITERAL", "EXPRESSION_DECIMAL_LITERAL", "EXPRESSION_STRING_LITERAL", 
		"EXPRESSION_NON_ZERO_DIGIT", "EXPRESSION_DIGIT", "EXPRESSION_NAME", "EXPRESSION_NameChar", 
		"EXPRESSION_NameStartChar"
	};


	public KatipLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "KatipLexer.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\26\u00e8\b\1\b\1"+
		"\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4"+
		"\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t"+
		"\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t"+
		"\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\7\2C\n\2\f\2\16\2F\13\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3"+
		"R\n\3\f\3\16\3U\13\3\3\3\5\3X\n\3\3\3\5\3[\n\3\3\4\3\4\5\4_\n\4\3\4\6"+
		"\4b\n\4\r\4\16\4c\3\5\3\5\3\5\3\5\3\5\3\6\3\6\5\6m\n\6\3\6\6\6p\n\6\r"+
		"\6\16\6q\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3"+
		"\n\3\n\3\n\3\13\3\13\7\13\u0088\n\13\f\13\16\13\u008b\13\13\3\f\3\f\3"+
		"\f\3\f\3\r\3\r\3\16\3\16\3\16\5\16\u0096\n\16\3\17\3\17\3\20\3\20\3\20"+
		"\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\5\25\u00af\n\25\3\26\5\26\u00b2\n\26\3\26\3\26\3"+
		"\26\7\26\u00b7\n\26\f\26\16\26\u00ba\13\26\5\26\u00bc\n\26\3\26\3\26\7"+
		"\26\u00c0\n\26\f\26\16\26\u00c3\13\26\3\26\6\26\u00c6\n\26\r\26\16\26"+
		"\u00c7\5\26\u00ca\n\26\3\27\3\27\7\27\u00ce\n\27\f\27\16\27\u00d1\13\27"+
		"\3\27\3\27\3\30\3\30\3\31\3\31\5\31\u00d9\n\31\3\32\3\32\7\32\u00dd\n"+
		"\32\f\32\16\32\u00e0\13\32\3\33\3\33\3\33\5\33\u00e5\n\33\3\34\3\34\4"+
		"DS\2\35\5\3\7\4\t\5\13\6\r\7\17\b\21\t\23\n\25\13\27\f\31\r\33\2\35\2"+
		"\37\2!\16#\17%\20\'\21)\22+\23-\24/\25\61\2\63\2\65\26\67\29\2\5\2\3\4"+
		"\13\4\2\13\13\"\"\3\2%%\3\2>>\5\2\13\f\17\17\"\"\3\2\62;\4\2C\\c|\4\2"+
		"--//\5\2$$))^^\3\2\63;\u00f6\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\3\17\3\2\2\2\3\21\3\2\2\2\3\23\3\2\2\2\3\25\3\2\2"+
		"\2\3\27\3\2\2\2\3\31\3\2\2\2\4!\3\2\2\2\4#\3\2\2\2\4%\3\2\2\2\4\'\3\2"+
		"\2\2\4)\3\2\2\2\4+\3\2\2\2\4-\3\2\2\2\4/\3\2\2\2\4\65\3\2\2\2\5;\3\2\2"+
		"\2\7L\3\2\2\2\ta\3\2\2\2\13e\3\2\2\2\rl\3\2\2\2\17s\3\2\2\2\21x\3\2\2"+
		"\2\23~\3\2\2\2\25\u0080\3\2\2\2\27\u0085\3\2\2\2\31\u008c\3\2\2\2\33\u0090"+
		"\3\2\2\2\35\u0095\3\2\2\2\37\u0097\3\2\2\2!\u0099\3\2\2\2#\u009d\3\2\2"+
		"\2%\u009f\3\2\2\2\'\u00a1\3\2\2\2)\u00a3\3\2\2\2+\u00ae\3\2\2\2-\u00b1"+
		"\3\2\2\2/\u00cb\3\2\2\2\61\u00d4\3\2\2\2\63\u00d8\3\2\2\2\65\u00da\3\2"+
		"\2\2\67\u00e4\3\2\2\29\u00e6\3\2\2\2;<\7>\2\2<=\7%\2\2=>\7#\2\2>?\7/\2"+
		"\2?@\7/\2\2@D\3\2\2\2AC\13\2\2\2BA\3\2\2\2CF\3\2\2\2DE\3\2\2\2DB\3\2\2"+
		"\2EG\3\2\2\2FD\3\2\2\2GH\7/\2\2HI\7/\2\2IJ\7%\2\2JK\7@\2\2K\6\3\2\2\2"+
		"LM\7>\2\2MN\7%\2\2NO\7#\2\2OS\3\2\2\2PR\13\2\2\2QP\3\2\2\2RU\3\2\2\2S"+
		"T\3\2\2\2SQ\3\2\2\2TZ\3\2\2\2US\3\2\2\2VX\7\17\2\2WV\3\2\2\2WX\3\2\2\2"+
		"XY\3\2\2\2Y[\7\f\2\2ZW\3\2\2\2Z[\3\2\2\2[\b\3\2\2\2\\b\t\2\2\2]_\7\17"+
		"\2\2^]\3\2\2\2^_\3\2\2\2_`\3\2\2\2`b\7\f\2\2a\\\3\2\2\2a^\3\2\2\2bc\3"+
		"\2\2\2ca\3\2\2\2cd\3\2\2\2d\n\3\2\2\2ef\7>\2\2fg\7%\2\2gh\3\2\2\2hi\b"+
		"\5\2\2i\f\3\2\2\2jk\7>\2\2km\n\3\2\2lj\3\2\2\2lm\3\2\2\2mo\3\2\2\2np\n"+
		"\4\2\2on\3\2\2\2pq\3\2\2\2qo\3\2\2\2qr\3\2\2\2r\16\3\2\2\2st\7%\2\2tu"+
		"\7@\2\2uv\3\2\2\2vw\b\7\3\2w\20\3\2\2\2xy\7%\2\2yz\7%\2\2z{\7@\2\2{|\3"+
		"\2\2\2|}\b\b\3\2}\22\3\2\2\2~\177\7%\2\2\177\24\3\2\2\2\u0080\u0081\7"+
		"?\2\2\u0081\u0082\7$\2\2\u0082\u0083\3\2\2\2\u0083\u0084\b\n\4\2\u0084"+
		"\26\3\2\2\2\u0085\u0089\5\37\17\2\u0086\u0088\5\35\16\2\u0087\u0086\3"+
		"\2\2\2\u0088\u008b\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a"+
		"\30\3\2\2\2\u008b\u0089\3\2\2\2\u008c\u008d\t\5\2\2\u008d\u008e\3\2\2"+
		"\2\u008e\u008f\b\f\5\2\u008f\32\3\2\2\2\u0090\u0091\t\6\2\2\u0091\34\3"+
		"\2\2\2\u0092\u0096\5\37\17\2\u0093\u0096\7a\2\2\u0094\u0096\5\33\r\2\u0095"+
		"\u0092\3\2\2\2\u0095\u0093\3\2\2\2\u0095\u0094\3\2\2\2\u0096\36\3\2\2"+
		"\2\u0097\u0098\t\7\2\2\u0098 \3\2\2\2\u0099\u009a\7$\2\2\u009a\u009b\3"+
		"\2\2\2\u009b\u009c\b\20\3\2\u009c\"\3\2\2\2\u009d\u009e\7.\2\2\u009e$"+
		"\3\2\2\2\u009f\u00a0\7\60\2\2\u00a0&\3\2\2\2\u00a1\u00a2\7*\2\2\u00a2"+
		"(\3\2\2\2\u00a3\u00a4\7+\2\2\u00a4*\3\2\2\2\u00a5\u00a6\7v\2\2\u00a6\u00a7"+
		"\7t\2\2\u00a7\u00a8\7w\2\2\u00a8\u00af\7g\2\2\u00a9\u00aa\7h\2\2\u00aa"+
		"\u00ab\7c\2\2\u00ab\u00ac\7n\2\2\u00ac\u00ad\7u\2\2\u00ad\u00af\7g\2\2"+
		"\u00ae\u00a5\3\2\2\2\u00ae\u00a9\3\2\2\2\u00af,\3\2\2\2\u00b0\u00b2\t"+
		"\b\2\2\u00b1\u00b0\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00bb\3\2\2\2\u00b3"+
		"\u00bc\7\62\2\2\u00b4\u00b8\5\61\30\2\u00b5\u00b7\5\63\31\2\u00b6\u00b5"+
		"\3\2\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9"+
		"\u00bc\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb\u00b3\3\2\2\2\u00bb\u00b4\3\2"+
		"\2\2\u00bc\u00c9\3\2\2\2\u00bd\u00c1\7\60\2\2\u00be\u00c0\5\63\31\2\u00bf"+
		"\u00be\3\2\2\2\u00c0\u00c3\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2"+
		"\2\2\u00c2\u00c5\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c4\u00c6\5\61\30\2\u00c5"+
		"\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7\u00c8\3\2"+
		"\2\2\u00c8\u00ca\3\2\2\2\u00c9\u00bd\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca"+
		".\3\2\2\2\u00cb\u00cf\7)\2\2\u00cc\u00ce\n\t\2\2\u00cd\u00cc\3\2\2\2\u00ce"+
		"\u00d1\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d2\3\2"+
		"\2\2\u00d1\u00cf\3\2\2\2\u00d2\u00d3\7)\2\2\u00d3\60\3\2\2\2\u00d4\u00d5"+
		"\t\n\2\2\u00d5\62\3\2\2\2\u00d6\u00d9\7\62\2\2\u00d7\u00d9\5\61\30\2\u00d8"+
		"\u00d6\3\2\2\2\u00d8\u00d7\3\2\2\2\u00d9\64\3\2\2\2\u00da\u00de\59\34"+
		"\2\u00db\u00dd\5\67\33\2\u00dc\u00db\3\2\2\2\u00dd\u00e0\3\2\2\2\u00de"+
		"\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df\66\3\2\2\2\u00e0\u00de\3\2\2"+
		"\2\u00e1\u00e5\59\34\2\u00e2\u00e5\7a\2\2\u00e3\u00e5\5\63\31\2\u00e4"+
		"\u00e1\3\2\2\2\u00e4\u00e2\3\2\2\2\u00e4\u00e3\3\2\2\2\u00e58\3\2\2\2"+
		"\u00e6\u00e7\t\7\2\2\u00e7:\3\2\2\2\33\2\3\4DSWZ^aclq\u0089\u0095\u00ae"+
		"\u00b1\u00b8\u00bb\u00c1\u00c7\u00c9\u00cf\u00d8\u00de\u00e4\6\7\3\2\6"+
		"\2\2\7\4\2\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}