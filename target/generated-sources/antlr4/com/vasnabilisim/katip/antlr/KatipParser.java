// Generated from KatipParser.g4 by ANTLR 4.4
package com.vasnabilisim.katip.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class KatipParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SEA_WS=3, EXPRESSION_PARANTHESIS_OPEN=15, EXPRESSION_DOT=14, EXPRESSION_BOOLEAN_LITERAL=17, 
		TEXT=5, EXPRESSION_NAME=20, ATTRIBUTE_VALUE_CLOSE=12, TAG_SHARP=8, TAG_NAME=10, 
		TAG_OPEN=4, BLOCK_COMMENT=1, LINE_COMMENT=2, TAG_CLOSE=6, EXPRESSION_PARANTHESIS_CLOSE=16, 
		EXPRESSION_STRING_LITERAL=19, TAG_WHITESPACE=11, TAG_EQUALS=9, TAG_SHARP_CLOSE=7, 
		EXPRESSION_COMMA=13, EXPRESSION_DECIMAL_LITERAL=18;
	public static final String[] tokenNames = {
		"<INVALID>", "BLOCK_COMMENT", "LINE_COMMENT", "SEA_WS", "'<#'", "TEXT", 
		"'#>'", "'##>'", "'#'", "'=\"'", "TAG_NAME", "TAG_WHITESPACE", "'\"'", 
		"','", "'.'", "'('", "')'", "EXPRESSION_BOOLEAN_LITERAL", "EXPRESSION_DECIMAL_LITERAL", 
		"EXPRESSION_STRING_LITERAL", "EXPRESSION_NAME"
	};
	public static final int
		RULE_katipDocument = 0, RULE_katipElements = 1, RULE_katipElement = 2, 
		RULE_katipTagName = 3, RULE_katipContent = 4, RULE_katipAttribute = 5, 
		RULE_katipAttributeName = 6, RULE_katipAttributeValue = 7, RULE_katipExpression = 8, 
		RULE_katipExpressionVariable = 9, RULE_katipExpressionVariableName = 10, 
		RULE_katipExpressionFunction = 11, RULE_katipFunctionName = 12, RULE_katipFunctionParameter = 13, 
		RULE_katipExpressionLiteral = 14, RULE_katipChardata = 15, RULE_katipMisc = 16, 
		RULE_katipComment = 17;
	public static final String[] ruleNames = {
		"katipDocument", "katipElements", "katipElement", "katipTagName", "katipContent", 
		"katipAttribute", "katipAttributeName", "katipAttributeValue", "katipExpression", 
		"katipExpressionVariable", "katipExpressionVariableName", "katipExpressionFunction", 
		"katipFunctionName", "katipFunctionParameter", "katipExpressionLiteral", 
		"katipChardata", "katipMisc", "katipComment"
	};

	@Override
	public String getGrammarFileName() { return "KatipParser.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public KatipParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class KatipDocumentContext extends ParserRuleContext {
		public KatipElementsContext katipElements(int i) {
			return getRuleContext(KatipElementsContext.class,i);
		}
		public List<KatipElementsContext> katipElements() {
			return getRuleContexts(KatipElementsContext.class);
		}
		public KatipDocumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipDocument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipDocument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipDocument(this);
		}
	}

	public final KatipDocumentContext katipDocument() throws RecognitionException {
		KatipDocumentContext _localctx = new KatipDocumentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_katipDocument);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BLOCK_COMMENT) | (1L << LINE_COMMENT) | (1L << SEA_WS) | (1L << TAG_OPEN) | (1L << TEXT))) != 0)) {
				{
				{
				setState(36); katipElements();
				}
				}
				setState(41);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipElementsContext extends ParserRuleContext {
		public List<KatipMiscContext> katipMisc() {
			return getRuleContexts(KatipMiscContext.class);
		}
		public KatipElementContext katipElement() {
			return getRuleContext(KatipElementContext.class,0);
		}
		public KatipMiscContext katipMisc(int i) {
			return getRuleContext(KatipMiscContext.class,i);
		}
		public KatipElementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipElements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipElements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipElements(this);
		}
	}

	public final KatipElementsContext katipElements() throws RecognitionException {
		KatipElementsContext _localctx = new KatipElementsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_katipElements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(45);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BLOCK_COMMENT) | (1L << LINE_COMMENT) | (1L << SEA_WS) | (1L << TEXT))) != 0)) {
				{
				{
				setState(42); katipMisc();
				}
				}
				setState(47);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(48); katipElement();
			setState(52);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(49); katipMisc();
					}
					} 
				}
				setState(54);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipElementContext extends ParserRuleContext {
		public TerminalNode TAG_OPEN(int i) {
			return getToken(KatipParser.TAG_OPEN, i);
		}
		public List<KatipAttributeContext> katipAttribute() {
			return getRuleContexts(KatipAttributeContext.class);
		}
		public KatipTagNameContext katipTagName(int i) {
			return getRuleContext(KatipTagNameContext.class,i);
		}
		public TerminalNode TAG_CLOSE(int i) {
			return getToken(KatipParser.TAG_CLOSE, i);
		}
		public List<TerminalNode> TAG_OPEN() { return getTokens(KatipParser.TAG_OPEN); }
		public KatipAttributeContext katipAttribute(int i) {
			return getRuleContext(KatipAttributeContext.class,i);
		}
		public List<TerminalNode> TAG_CLOSE() { return getTokens(KatipParser.TAG_CLOSE); }
		public List<KatipTagNameContext> katipTagName() {
			return getRuleContexts(KatipTagNameContext.class);
		}
		public KatipContentContext katipContent() {
			return getRuleContext(KatipContentContext.class,0);
		}
		public TerminalNode TAG_SHARP_CLOSE() { return getToken(KatipParser.TAG_SHARP_CLOSE, 0); }
		public TerminalNode TAG_SHARP() { return getToken(KatipParser.TAG_SHARP, 0); }
		public KatipElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipElement(this);
		}
	}

	public final KatipElementContext katipElement() throws RecognitionException {
		KatipElementContext _localctx = new KatipElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_katipElement);
		int _la;
		try {
			setState(80);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(55); match(TAG_OPEN);
				setState(56); katipTagName();
				setState(60);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TAG_NAME) {
					{
					{
					setState(57); katipAttribute();
					}
					}
					setState(62);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(63); match(TAG_CLOSE);
				setState(64); katipContent();
				setState(65); match(TAG_OPEN);
				setState(66); match(TAG_SHARP);
				setState(67); katipTagName();
				setState(68); match(TAG_CLOSE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(70); match(TAG_OPEN);
				setState(71); katipTagName();
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==TAG_NAME) {
					{
					{
					setState(72); katipAttribute();
					}
					}
					setState(77);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(78); match(TAG_SHARP_CLOSE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipTagNameContext extends ParserRuleContext {
		public TerminalNode TAG_NAME() { return getToken(KatipParser.TAG_NAME, 0); }
		public KatipTagNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipTagName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipTagName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipTagName(this);
		}
	}

	public final KatipTagNameContext katipTagName() throws RecognitionException {
		KatipTagNameContext _localctx = new KatipTagNameContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_katipTagName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82); match(TAG_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipContentContext extends ParserRuleContext {
		public KatipElementContext katipElement(int i) {
			return getRuleContext(KatipElementContext.class,i);
		}
		public List<KatipCommentContext> katipComment() {
			return getRuleContexts(KatipCommentContext.class);
		}
		public List<KatipElementContext> katipElement() {
			return getRuleContexts(KatipElementContext.class);
		}
		public KatipCommentContext katipComment(int i) {
			return getRuleContext(KatipCommentContext.class,i);
		}
		public KatipChardataContext katipChardata(int i) {
			return getRuleContext(KatipChardataContext.class,i);
		}
		public List<KatipChardataContext> katipChardata() {
			return getRuleContexts(KatipChardataContext.class);
		}
		public KatipContentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipContent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipContent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipContent(this);
		}
	}

	public final KatipContentContext katipContent() throws RecognitionException {
		KatipContentContext _localctx = new KatipContentContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_katipContent);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			_la = _input.LA(1);
			if (_la==SEA_WS || _la==TEXT) {
				{
				setState(84); katipChardata();
				}
			}

			setState(96);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(89);
					switch (_input.LA(1)) {
					case TAG_OPEN:
						{
						setState(87); katipElement();
						}
						break;
					case BLOCK_COMMENT:
					case LINE_COMMENT:
						{
						setState(88); katipComment();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(92);
					_la = _input.LA(1);
					if (_la==SEA_WS || _la==TEXT) {
						{
						setState(91); katipChardata();
						}
					}

					}
					} 
				}
				setState(98);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipAttributeContext extends ParserRuleContext {
		public KatipAttributeValueContext katipAttributeValue() {
			return getRuleContext(KatipAttributeValueContext.class,0);
		}
		public KatipAttributeNameContext katipAttributeName() {
			return getRuleContext(KatipAttributeNameContext.class,0);
		}
		public TerminalNode TAG_EQUALS() { return getToken(KatipParser.TAG_EQUALS, 0); }
		public KatipAttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipAttribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipAttribute(this);
		}
	}

	public final KatipAttributeContext katipAttribute() throws RecognitionException {
		KatipAttributeContext _localctx = new KatipAttributeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_katipAttribute);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99); katipAttributeName();
			setState(100); match(TAG_EQUALS);
			setState(101); katipAttributeValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipAttributeNameContext extends ParserRuleContext {
		public TerminalNode TAG_NAME() { return getToken(KatipParser.TAG_NAME, 0); }
		public KatipAttributeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipAttributeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipAttributeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipAttributeName(this);
		}
	}

	public final KatipAttributeNameContext katipAttributeName() throws RecognitionException {
		KatipAttributeNameContext _localctx = new KatipAttributeNameContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_katipAttributeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(103); match(TAG_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipAttributeValueContext extends ParserRuleContext {
		public TerminalNode ATTRIBUTE_VALUE_CLOSE() { return getToken(KatipParser.ATTRIBUTE_VALUE_CLOSE, 0); }
		public KatipExpressionContext katipExpression() {
			return getRuleContext(KatipExpressionContext.class,0);
		}
		public KatipAttributeValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipAttributeValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipAttributeValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipAttributeValue(this);
		}
	}

	public final KatipAttributeValueContext katipAttributeValue() throws RecognitionException {
		KatipAttributeValueContext _localctx = new KatipAttributeValueContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_katipAttributeValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105); katipExpression();
			setState(106); match(ATTRIBUTE_VALUE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipExpressionContext extends ParserRuleContext {
		public KatipExpressionFunctionContext katipExpressionFunction() {
			return getRuleContext(KatipExpressionFunctionContext.class,0);
		}
		public KatipExpressionLiteralContext katipExpressionLiteral() {
			return getRuleContext(KatipExpressionLiteralContext.class,0);
		}
		public KatipExpressionVariableContext katipExpressionVariable() {
			return getRuleContext(KatipExpressionVariableContext.class,0);
		}
		public KatipExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipExpression(this);
		}
	}

	public final KatipExpressionContext katipExpression() throws RecognitionException {
		KatipExpressionContext _localctx = new KatipExpressionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_katipExpression);
		try {
			setState(111);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(108); katipExpressionVariable();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(109); katipExpressionFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(110); katipExpressionLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipExpressionVariableContext extends ParserRuleContext {
		public KatipExpressionVariableNameContext katipExpressionVariableName(int i) {
			return getRuleContext(KatipExpressionVariableNameContext.class,i);
		}
		public TerminalNode EXPRESSION_DOT(int i) {
			return getToken(KatipParser.EXPRESSION_DOT, i);
		}
		public List<TerminalNode> EXPRESSION_DOT() { return getTokens(KatipParser.EXPRESSION_DOT); }
		public List<KatipExpressionVariableNameContext> katipExpressionVariableName() {
			return getRuleContexts(KatipExpressionVariableNameContext.class);
		}
		public KatipExpressionVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipExpressionVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipExpressionVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipExpressionVariable(this);
		}
	}

	public final KatipExpressionVariableContext katipExpressionVariable() throws RecognitionException {
		KatipExpressionVariableContext _localctx = new KatipExpressionVariableContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_katipExpressionVariable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113); katipExpressionVariableName();
			setState(118);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==EXPRESSION_DOT) {
				{
				{
				setState(114); match(EXPRESSION_DOT);
				setState(115); katipExpressionVariableName();
				}
				}
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipExpressionVariableNameContext extends ParserRuleContext {
		public TerminalNode EXPRESSION_NAME() { return getToken(KatipParser.EXPRESSION_NAME, 0); }
		public KatipExpressionVariableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipExpressionVariableName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipExpressionVariableName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipExpressionVariableName(this);
		}
	}

	public final KatipExpressionVariableNameContext katipExpressionVariableName() throws RecognitionException {
		KatipExpressionVariableNameContext _localctx = new KatipExpressionVariableNameContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_katipExpressionVariableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121); match(EXPRESSION_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipExpressionFunctionContext extends ParserRuleContext {
		public List<KatipFunctionParameterContext> katipFunctionParameter() {
			return getRuleContexts(KatipFunctionParameterContext.class);
		}
		public TerminalNode EXPRESSION_PARANTHESIS_OPEN() { return getToken(KatipParser.EXPRESSION_PARANTHESIS_OPEN, 0); }
		public TerminalNode EXPRESSION_PARANTHESIS_CLOSE() { return getToken(KatipParser.EXPRESSION_PARANTHESIS_CLOSE, 0); }
		public KatipFunctionNameContext katipFunctionName() {
			return getRuleContext(KatipFunctionNameContext.class,0);
		}
		public List<TerminalNode> EXPRESSION_COMMA() { return getTokens(KatipParser.EXPRESSION_COMMA); }
		public KatipFunctionParameterContext katipFunctionParameter(int i) {
			return getRuleContext(KatipFunctionParameterContext.class,i);
		}
		public TerminalNode EXPRESSION_COMMA(int i) {
			return getToken(KatipParser.EXPRESSION_COMMA, i);
		}
		public KatipExpressionFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipExpressionFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipExpressionFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipExpressionFunction(this);
		}
	}

	public final KatipExpressionFunctionContext katipExpressionFunction() throws RecognitionException {
		KatipExpressionFunctionContext _localctx = new KatipExpressionFunctionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_katipExpressionFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123); katipFunctionName();
			setState(124); match(EXPRESSION_PARANTHESIS_OPEN);
			setState(135);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EXPRESSION_BOOLEAN_LITERAL) | (1L << EXPRESSION_DECIMAL_LITERAL) | (1L << EXPRESSION_STRING_LITERAL) | (1L << EXPRESSION_NAME))) != 0)) {
				{
				{
				setState(125); katipFunctionParameter();
				setState(130);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==EXPRESSION_COMMA) {
					{
					{
					setState(126); match(EXPRESSION_COMMA);
					setState(127); katipFunctionParameter();
					}
					}
					setState(132);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				setState(137);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(138); match(EXPRESSION_PARANTHESIS_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipFunctionNameContext extends ParserRuleContext {
		public TerminalNode EXPRESSION_NAME() { return getToken(KatipParser.EXPRESSION_NAME, 0); }
		public KatipFunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipFunctionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipFunctionName(this);
		}
	}

	public final KatipFunctionNameContext katipFunctionName() throws RecognitionException {
		KatipFunctionNameContext _localctx = new KatipFunctionNameContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_katipFunctionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140); match(EXPRESSION_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipFunctionParameterContext extends ParserRuleContext {
		public KatipExpressionFunctionContext katipExpressionFunction() {
			return getRuleContext(KatipExpressionFunctionContext.class,0);
		}
		public KatipExpressionLiteralContext katipExpressionLiteral() {
			return getRuleContext(KatipExpressionLiteralContext.class,0);
		}
		public KatipExpressionVariableContext katipExpressionVariable() {
			return getRuleContext(KatipExpressionVariableContext.class,0);
		}
		public KatipFunctionParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipFunctionParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipFunctionParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipFunctionParameter(this);
		}
	}

	public final KatipFunctionParameterContext katipFunctionParameter() throws RecognitionException {
		KatipFunctionParameterContext _localctx = new KatipFunctionParameterContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_katipFunctionParameter);
		try {
			setState(145);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(142); katipExpressionVariable();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(143); katipExpressionFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(144); katipExpressionLiteral();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipExpressionLiteralContext extends ParserRuleContext {
		public TerminalNode EXPRESSION_DECIMAL_LITERAL() { return getToken(KatipParser.EXPRESSION_DECIMAL_LITERAL, 0); }
		public TerminalNode EXPRESSION_BOOLEAN_LITERAL() { return getToken(KatipParser.EXPRESSION_BOOLEAN_LITERAL, 0); }
		public TerminalNode EXPRESSION_STRING_LITERAL() { return getToken(KatipParser.EXPRESSION_STRING_LITERAL, 0); }
		public KatipExpressionLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipExpressionLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipExpressionLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipExpressionLiteral(this);
		}
	}

	public final KatipExpressionLiteralContext katipExpressionLiteral() throws RecognitionException {
		KatipExpressionLiteralContext _localctx = new KatipExpressionLiteralContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_katipExpressionLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(147);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EXPRESSION_BOOLEAN_LITERAL) | (1L << EXPRESSION_DECIMAL_LITERAL) | (1L << EXPRESSION_STRING_LITERAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipChardataContext extends ParserRuleContext {
		public TerminalNode TEXT() { return getToken(KatipParser.TEXT, 0); }
		public TerminalNode SEA_WS() { return getToken(KatipParser.SEA_WS, 0); }
		public KatipChardataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipChardata; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipChardata(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipChardata(this);
		}
	}

	public final KatipChardataContext katipChardata() throws RecognitionException {
		KatipChardataContext _localctx = new KatipChardataContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_katipChardata);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			_la = _input.LA(1);
			if ( !(_la==SEA_WS || _la==TEXT) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipMiscContext extends ParserRuleContext {
		public TerminalNode SEA_WS() { return getToken(KatipParser.SEA_WS, 0); }
		public KatipCommentContext katipComment() {
			return getRuleContext(KatipCommentContext.class,0);
		}
		public KatipChardataContext katipChardata() {
			return getRuleContext(KatipChardataContext.class,0);
		}
		public KatipMiscContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipMisc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipMisc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipMisc(this);
		}
	}

	public final KatipMiscContext katipMisc() throws RecognitionException {
		KatipMiscContext _localctx = new KatipMiscContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_katipMisc);
		try {
			setState(154);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(151); katipComment();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(152); katipChardata();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(153); match(SEA_WS);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KatipCommentContext extends ParserRuleContext {
		public TerminalNode BLOCK_COMMENT() { return getToken(KatipParser.BLOCK_COMMENT, 0); }
		public TerminalNode LINE_COMMENT() { return getToken(KatipParser.LINE_COMMENT, 0); }
		public KatipCommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_katipComment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).enterKatipComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof KatipParserListener ) ((KatipParserListener)listener).exitKatipComment(this);
		}
	}

	public final KatipCommentContext katipComment() throws RecognitionException {
		KatipCommentContext _localctx = new KatipCommentContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_katipComment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(156);
			_la = _input.LA(1);
			if ( !(_la==BLOCK_COMMENT || _la==LINE_COMMENT) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\26\u00a1\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\3\2\7\2(\n\2\f\2\16\2+\13\2\3\3\7\3.\n\3\f\3\16\3\61\13\3\3"+
		"\3\3\3\7\3\65\n\3\f\3\16\38\13\3\3\4\3\4\3\4\7\4=\n\4\f\4\16\4@\13\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4L\n\4\f\4\16\4O\13\4\3\4\3\4"+
		"\5\4S\n\4\3\5\3\5\3\6\5\6X\n\6\3\6\3\6\5\6\\\n\6\3\6\5\6_\n\6\7\6a\n\6"+
		"\f\6\16\6d\13\6\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\5\nr\n"+
		"\n\3\13\3\13\3\13\7\13w\n\13\f\13\16\13z\13\13\3\f\3\f\3\r\3\r\3\r\3\r"+
		"\3\r\7\r\u0083\n\r\f\r\16\r\u0086\13\r\7\r\u0088\n\r\f\r\16\r\u008b\13"+
		"\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\5\17\u0094\n\17\3\20\3\20\3\21\3\21"+
		"\3\22\3\22\3\22\5\22\u009d\n\22\3\23\3\23\3\23\2\2\24\2\4\6\b\n\f\16\20"+
		"\22\24\26\30\32\34\36 \"$\2\5\3\2\23\25\4\2\5\5\7\7\3\2\3\4\u00a1\2)\3"+
		"\2\2\2\4/\3\2\2\2\6R\3\2\2\2\bT\3\2\2\2\nW\3\2\2\2\fe\3\2\2\2\16i\3\2"+
		"\2\2\20k\3\2\2\2\22q\3\2\2\2\24s\3\2\2\2\26{\3\2\2\2\30}\3\2\2\2\32\u008e"+
		"\3\2\2\2\34\u0093\3\2\2\2\36\u0095\3\2\2\2 \u0097\3\2\2\2\"\u009c\3\2"+
		"\2\2$\u009e\3\2\2\2&(\5\4\3\2\'&\3\2\2\2(+\3\2\2\2)\'\3\2\2\2)*\3\2\2"+
		"\2*\3\3\2\2\2+)\3\2\2\2,.\5\"\22\2-,\3\2\2\2.\61\3\2\2\2/-\3\2\2\2/\60"+
		"\3\2\2\2\60\62\3\2\2\2\61/\3\2\2\2\62\66\5\6\4\2\63\65\5\"\22\2\64\63"+
		"\3\2\2\2\658\3\2\2\2\66\64\3\2\2\2\66\67\3\2\2\2\67\5\3\2\2\28\66\3\2"+
		"\2\29:\7\6\2\2:>\5\b\5\2;=\5\f\7\2<;\3\2\2\2=@\3\2\2\2><\3\2\2\2>?\3\2"+
		"\2\2?A\3\2\2\2@>\3\2\2\2AB\7\b\2\2BC\5\n\6\2CD\7\6\2\2DE\7\n\2\2EF\5\b"+
		"\5\2FG\7\b\2\2GS\3\2\2\2HI\7\6\2\2IM\5\b\5\2JL\5\f\7\2KJ\3\2\2\2LO\3\2"+
		"\2\2MK\3\2\2\2MN\3\2\2\2NP\3\2\2\2OM\3\2\2\2PQ\7\t\2\2QS\3\2\2\2R9\3\2"+
		"\2\2RH\3\2\2\2S\7\3\2\2\2TU\7\f\2\2U\t\3\2\2\2VX\5 \21\2WV\3\2\2\2WX\3"+
		"\2\2\2Xb\3\2\2\2Y\\\5\6\4\2Z\\\5$\23\2[Y\3\2\2\2[Z\3\2\2\2\\^\3\2\2\2"+
		"]_\5 \21\2^]\3\2\2\2^_\3\2\2\2_a\3\2\2\2`[\3\2\2\2ad\3\2\2\2b`\3\2\2\2"+
		"bc\3\2\2\2c\13\3\2\2\2db\3\2\2\2ef\5\16\b\2fg\7\13\2\2gh\5\20\t\2h\r\3"+
		"\2\2\2ij\7\f\2\2j\17\3\2\2\2kl\5\22\n\2lm\7\16\2\2m\21\3\2\2\2nr\5\24"+
		"\13\2or\5\30\r\2pr\5\36\20\2qn\3\2\2\2qo\3\2\2\2qp\3\2\2\2r\23\3\2\2\2"+
		"sx\5\26\f\2tu\7\20\2\2uw\5\26\f\2vt\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3\2"+
		"\2\2y\25\3\2\2\2zx\3\2\2\2{|\7\26\2\2|\27\3\2\2\2}~\5\32\16\2~\u0089\7"+
		"\21\2\2\177\u0084\5\34\17\2\u0080\u0081\7\17\2\2\u0081\u0083\5\34\17\2"+
		"\u0082\u0080\3\2\2\2\u0083\u0086\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085"+
		"\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0087\177\3\2\2\2\u0088"+
		"\u008b\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u008c\3\2"+
		"\2\2\u008b\u0089\3\2\2\2\u008c\u008d\7\22\2\2\u008d\31\3\2\2\2\u008e\u008f"+
		"\7\26\2\2\u008f\33\3\2\2\2\u0090\u0094\5\24\13\2\u0091\u0094\5\30\r\2"+
		"\u0092\u0094\5\36\20\2\u0093\u0090\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0092"+
		"\3\2\2\2\u0094\35\3\2\2\2\u0095\u0096\t\2\2\2\u0096\37\3\2\2\2\u0097\u0098"+
		"\t\3\2\2\u0098!\3\2\2\2\u0099\u009d\5$\23\2\u009a\u009d\5 \21\2\u009b"+
		"\u009d\7\5\2\2\u009c\u0099\3\2\2\2\u009c\u009a\3\2\2\2\u009c\u009b\3\2"+
		"\2\2\u009d#\3\2\2\2\u009e\u009f\t\4\2\2\u009f%\3\2\2\2\22)/\66>MRW[^b"+
		"qx\u0084\u0089\u0093\u009c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}