// Generated from KatipParser.g4 by ANTLR 4.4
package com.vasnabilisim.katip.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link KatipParser}.
 */
public interface KatipParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipContent}.
	 * @param ctx the parse tree
	 */
	void enterKatipContent(@NotNull KatipParser.KatipContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipContent}.
	 * @param ctx the parse tree
	 */
	void exitKatipContent(@NotNull KatipParser.KatipContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipElements}.
	 * @param ctx the parse tree
	 */
	void enterKatipElements(@NotNull KatipParser.KatipElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipElements}.
	 * @param ctx the parse tree
	 */
	void exitKatipElements(@NotNull KatipParser.KatipElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipAttributeValue}.
	 * @param ctx the parse tree
	 */
	void enterKatipAttributeValue(@NotNull KatipParser.KatipAttributeValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipAttributeValue}.
	 * @param ctx the parse tree
	 */
	void exitKatipAttributeValue(@NotNull KatipParser.KatipAttributeValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterKatipFunctionName(@NotNull KatipParser.KatipFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitKatipFunctionName(@NotNull KatipParser.KatipFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipTagName}.
	 * @param ctx the parse tree
	 */
	void enterKatipTagName(@NotNull KatipParser.KatipTagNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipTagName}.
	 * @param ctx the parse tree
	 */
	void exitKatipTagName(@NotNull KatipParser.KatipTagNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipElement}.
	 * @param ctx the parse tree
	 */
	void enterKatipElement(@NotNull KatipParser.KatipElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipElement}.
	 * @param ctx the parse tree
	 */
	void exitKatipElement(@NotNull KatipParser.KatipElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipExpressionVariable}.
	 * @param ctx the parse tree
	 */
	void enterKatipExpressionVariable(@NotNull KatipParser.KatipExpressionVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipExpressionVariable}.
	 * @param ctx the parse tree
	 */
	void exitKatipExpressionVariable(@NotNull KatipParser.KatipExpressionVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipExpression}.
	 * @param ctx the parse tree
	 */
	void enterKatipExpression(@NotNull KatipParser.KatipExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipExpression}.
	 * @param ctx the parse tree
	 */
	void exitKatipExpression(@NotNull KatipParser.KatipExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipMisc}.
	 * @param ctx the parse tree
	 */
	void enterKatipMisc(@NotNull KatipParser.KatipMiscContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipMisc}.
	 * @param ctx the parse tree
	 */
	void exitKatipMisc(@NotNull KatipParser.KatipMiscContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipExpressionVariableName}.
	 * @param ctx the parse tree
	 */
	void enterKatipExpressionVariableName(@NotNull KatipParser.KatipExpressionVariableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipExpressionVariableName}.
	 * @param ctx the parse tree
	 */
	void exitKatipExpressionVariableName(@NotNull KatipParser.KatipExpressionVariableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipExpressionFunction}.
	 * @param ctx the parse tree
	 */
	void enterKatipExpressionFunction(@NotNull KatipParser.KatipExpressionFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipExpressionFunction}.
	 * @param ctx the parse tree
	 */
	void exitKatipExpressionFunction(@NotNull KatipParser.KatipExpressionFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipAttributeName}.
	 * @param ctx the parse tree
	 */
	void enterKatipAttributeName(@NotNull KatipParser.KatipAttributeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipAttributeName}.
	 * @param ctx the parse tree
	 */
	void exitKatipAttributeName(@NotNull KatipParser.KatipAttributeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipChardata}.
	 * @param ctx the parse tree
	 */
	void enterKatipChardata(@NotNull KatipParser.KatipChardataContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipChardata}.
	 * @param ctx the parse tree
	 */
	void exitKatipChardata(@NotNull KatipParser.KatipChardataContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipAttribute}.
	 * @param ctx the parse tree
	 */
	void enterKatipAttribute(@NotNull KatipParser.KatipAttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipAttribute}.
	 * @param ctx the parse tree
	 */
	void exitKatipAttribute(@NotNull KatipParser.KatipAttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipComment}.
	 * @param ctx the parse tree
	 */
	void enterKatipComment(@NotNull KatipParser.KatipCommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipComment}.
	 * @param ctx the parse tree
	 */
	void exitKatipComment(@NotNull KatipParser.KatipCommentContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipDocument}.
	 * @param ctx the parse tree
	 */
	void enterKatipDocument(@NotNull KatipParser.KatipDocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipDocument}.
	 * @param ctx the parse tree
	 */
	void exitKatipDocument(@NotNull KatipParser.KatipDocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipExpressionLiteral}.
	 * @param ctx the parse tree
	 */
	void enterKatipExpressionLiteral(@NotNull KatipParser.KatipExpressionLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipExpressionLiteral}.
	 * @param ctx the parse tree
	 */
	void exitKatipExpressionLiteral(@NotNull KatipParser.KatipExpressionLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link KatipParser#katipFunctionParameter}.
	 * @param ctx the parse tree
	 */
	void enterKatipFunctionParameter(@NotNull KatipParser.KatipFunctionParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link KatipParser#katipFunctionParameter}.
	 * @param ctx the parse tree
	 */
	void exitKatipFunctionParameter(@NotNull KatipParser.KatipFunctionParameterContext ctx);
}